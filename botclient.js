const Discord = require('discord.js');

/** @type {import('discord.js').Client[]} */
let clients = [];

const combined = {
  emojis: undefined,
  channels: undefined,
  guilds: undefined,
};

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

/**
 * The service operates multiple bots, so rather than using the discord.js Client object directly
 * we need a wrapper object.
 */
module.exports = {

  /**
   * Initializes bot instances.
   *
   * @returns {Promise<void>} promise that resolves once initialization is complete
   */
  async init() {
    for (let i = 0; i < clients.length; i++) {
      await clients[i].destroy();
    }

    clients = [];

    const tokens = JSON.parse(process.env.BOT_TOKENS);
    for (let i = 0; i < tokens.length; i++) {
      const token = tokens[i];

      const client = new Discord.Client({
        disableEveryone: true,
        restTimeOffset: 750,
        retryLimit: 2,
      });

      console.log(`Logging into Client (${i+1}/${tokens.length})`);

      await client.login(token);
      clients.push(client);
    }

    await sleep(2000);
    await this.populateCombined();
  },

  async populateCombined() {
    combined.emojis = undefined;
    combined.channels = undefined;
    combined.guilds = undefined;

    for (let i = 0; i < clients.length; i++) {
      const client = clients[i];

      try {
        if (combined.emojis) {
          combined.emojis = combined.emojis.concat(client.emojis.cache);
        } else {
          combined.emojis = client.emojis.cache;
        }
      } catch (ex) {
        console.log('Failed to get emojis for client', ex);
      }

      try {
        if (combined.channels) {
          combined.channels = combined.channels.concat(client.channels.cache);
        } else {
          combined.channels = client.channels.cache;
        }
      } catch (ex) {
        console.log('Failed to get channels for client', ex);
      }

      try {
        if (combined.guilds) {
          combined.guilds = combined.guilds.concat(client.guilds.cache);
        } else {
          combined.guilds = client.guilds.cache;
        }
      } catch (ex) {
        console.log('Failed to get guilds for client', ex);
      }

      await sleep(500);
    }
  },

  /**
   * @returns {import('discord.js').Emoji}
   */
  getEmojiById(emojiId) {
    return combined.emojis && (combined.emojis.get(emojiId) || combined.emojis.find(emoji => emoji.id === emojiId));
  },

  /**
   * @returns {import('discord.js').Emoji}
   */
  getEmojiByName(emojiName) {
    return combined.emojis && combined.emojis.find(emoji => emoji.name === emojiName);
  },

  /**
   * @returns {import('discord.js').GuildChannel}
   */
  getChannelById(channelId) {
    return combined.channels && combined.channels.get(channelId);
  },

  /**
   * @returns {import('discord.js').Guild}
   */
  getGuildById(guildId) {
    return combined.guilds && combined.guilds.get(guildId);
  },

  /**
   * Finds the correct bot to send the message (based on `preferredBotId`) and sends the message
   * to the user
   *
   * @param {String} preferredBotId id of the bot to send message
   * @param {String} userDiscordId user to send message to
   * @param {String|Object} content the contents of the message
   * @returns {Promise<boolean>}
   */
  async sendMessageToUser(preferredBotId, userDiscordId, content) {
    for (let i = 0; i < clients.length; i++) {
      const client = clients[i];

      if (!!preferredBotId && client.user.id !== preferredBotId) {
        continue;
      }

      const user = await client.users.fetch(userDiscordId);

      if (user) {
        try {
          await user.send(typeof content === 'string' ? content : {embed: content});
          return true;
        } catch (err) {
          console.error(err);
          return false;
        }
      }
    }

    if (!!preferredBotId) {
      // If no match for preferredBotId, then try to send again without any bot id -- which will
      // just use the first bot that's capable of sending the message.
      return this.sendMessageToUser(null, userDiscordId, content);
    }

    return false;
  }
};