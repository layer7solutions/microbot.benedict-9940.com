require('dotenv').config();

const sslcreds = require('./sslcreds.js');
const express = require('express');
const spdy = require('spdy');

const app = express();
const bot = require('./botclient.js');

const fromEntries = require('object.fromentries');

process.on('uncaughtException', error => {
  console.error((new Date).toUTCString() + ' uncaughtException:', error.message);
  console.error(error.stack);
  process.exit(1);
});

process.on('unhandledRejection', error => {
	console.error('Unhandled promise rejection:', error);
  process.exit(1);
});

(async () => {
  console.log('Initializing connection to Discord bots...');
  await bot.init();

  console.log('Loading API routes...');

  app.use(require('body-parser').json());

  app.use((req, res, next) => {
    if (req.headers['x-api-key'] !== process.env.MICROBOTKEY && req.query.key !== process.env.MICROBOTKEY) {
      return res.status(403).send('Forbidden.');
    }
    res.header({
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers':
        'Origin, Content-Type, Accept, User-Agent, X-CSRF-Token, X-Requested-With, Authorization',
      'Access-Control-Allow-Credentials': 'true',
    });
    next();
  });

  app.route('/ping').get((req,res) => {
    res.send('Pong!');
  });

  app.route('/emojis').get((req,res) => {
    if (req.query.id) {
      res.json(bot.getEmojiById(req.query.id));
    } else if (req.query.name) {
      res.json(bot.getEmojiByName(req.query.name));
    } else {
      res.json(null);
    }
  });

  app.route('/emojis/:id').get((req,res) => {
    res.json(bot.getEmojiById(req.params.id));
  });

  app.route('/channels/:id').get((req,res) => {
    res.json(bot.getChannelById(req.params.id));
  });

  app.route('/guilds/:id').get((req,res) => {
    res.json(bot.getGuildById(req.params.id));
  });

  app.route('/guilds/:id/roles').get((req,res) => {
    const guild = bot.getGuildById(req.params.id);
    if (!guild) return res.json({});
    res.json(fromEntries(guild.roles.cache));
  });
  app.route('/guilds/:id/roles/:rid').get((req,res) => {
    const guild = bot.getGuildById(req.params.id);
    if (!guild) return res.json({});
    res.json(guild.roles.cache.get(req.params.rid));
  });

  app.route('/guilds/:id/emojis').get((req,res) => {
    const guild = bot.getGuildById(req.params.id);
    if (!guild) return res.json({});
    res.json(fromEntries(guild.emojis.cache));
  });
  app.route('/guilds/:id/emojis/:eid').get((req,res) => {
    const guild = bot.getGuildById(req.params.id);
    if (!guild) return res.json({});
    res.json(guild.emojis.cache.get(req.params.eid));
  });

  app.route('/guilds/:id/channels').get((req,res) => {
    const guild = bot.getGuildById(req.params.id);
    if (!guild) return res.json({});
    res.json(fromEntries(guild.channels.cache));
  });
  app.route('/guilds/:id/channels/:cid').get((req,res) => {
    const guild = bot.getGuildById(req.params.id);
    if (!guild) return res.json({});
    res.json(guild.channels.cache.get(req.params.cid));
  });

  app.route('/guilds/:id/text-channels').get((req,res) => {
    const guild = bot.getGuildById(req.params.id);
    if (!guild) return res.json({});
    res.json(fromEntries(guild.channels.cache.filter(c => c.type === 'text')));
  });
  app.route('/guilds/:id/text-channels/:cid').get((req,res) => {
    const guild = bot.getGuildById(req.params.id);
    if (!guild) return res.json({});
    res.json(guild.channels.cache.filter(c => c.type === 'text').get(req.params.cid));
  });

  app.route('/guilds/:id/voice-channels').get((req,res) => {
    const guild = bot.getGuildById(req.params.id);
    if (!guild) return res.json({});
    res.json(fromEntries(guild.channels.cache.filter(c => c.type === 'voice')));
  });
  app.route('/guilds/:id/voice-channels/:cid').get((req,res) => {
    const guild = bot.getGuildById(req.params.id);
    if (!guild) return res.json({});
    res.json(guild.channels.cache.filter(c => c.type === 'voice').get(req.params.cid));
  });

  app.route('/guilds/:id/news-channels').get((req,res) => {
    const guild = bot.getGuildById(req.params.id);
    if (!guild) return res.json({});
    res.json(fromEntries(guild.channels.cache.filter(c => c.type === 'news')));
  });
  app.route('/guilds/:id/news-channels/:cid').get((req,res) => {
    const guild = bot.getGuildById(req.params.id);
    if (!guild) return res.json({});
    res.json(guild.channels.cache.filter(c => c.type === 'news').get(req.params.cid));
  });

  app.route('/guilds/:id/store-channels').get((req,res) => {
    const guild = bot.getGuildById(req.params.id);
    if (!guild) return res.json({});
    res.json(fromEntries(guild.channels.cache.filter(c => c.type === 'store')));
  });
  app.route('/guilds/:id/store-channels/:cid').get((req,res) => {
    const guild = bot.getGuildById(req.params.id);
    if (!guild) return res.json({});
    res.json(guild.channels.cache.filter(c => c.type === 'store').get(req.params.cid));
  });

  app.route('/guilds/:id/categories').get((req,res) => {
    const guild = bot.getGuildById(req.params.id);
    if (!guild) return res.json({});
    res.json(fromEntries(guild.channels.cache.filter(c => c.type === 'category')));
  });
  app.route('/guilds/:id/categories/:cid').get((req,res) => {
    const guild = bot.getGuildById(req.params.id);
    if (!guild) return res.json({});
    res.json(guild.channels.cache.filter(c => c.type === 'category').get(req.params.cid));
  });

  app.route('/sendDirectMessage').post(async (req,res) => {
    res.json(await bot.sendMessageToUser(req.body.bot_id, req.body.user_id, req.body.content));
  });

  app.route('/admin/init').post(async (req,res) => {
    res.json(await bot.init());
  });
  app.route('/admin/populateCombined').post(async (req,res) => {
    res.json(await bot.populateCombined());
  });

  app.listen(process.env.HTTP_PORT, error => {
    if (error) {
      console.error(error)
      return process.exit(1)
    }
    console.log(`HTTP/2 Server running on port ${process.env.HTTP_PORT}`);
  });

  spdy.createServer(sslcreds, app).listen(process.env.HTTPS_PORT, error => {
    if (error) {
      console.error(error)
      return process.exit(1)
    }
    console.log(`HTTPS/2 Server running on port ${process.env.HTTPS_PORT}`);
  });
})();